﻿## PHP Version
- PHP 5.6 +

------------
## เริ่มต้นใช้งาน

-	 [Download File](https://bitbucket.org/MoneySpace/money-space-for-e-commerce-marketplace/downloads/ "ดาวโหลดไฟล์")
-	แตกไฟล์ ZIp
- นำไปวางใน Project


##  สร้าง Secret ID , Secret Key
- เข้าสู่ระบบ [https://www.moneyspace.net](https://www.moneyspace.net/) 
- เมนู Webhooks
- กรอกโดเมน และ Webhook


![enter image description here](https://lh3.googleusercontent.com/8tYnjKBa1T6Ucv81gB316U03Ay2oUDs_iD4wvk-ijcC4XQ_I9Lp2-kviPlis_CSOJIspXxDwWkE)

![
](https://lh3.googleusercontent.com/3EJ0nrsm7wjOBaYm4a4sYelIBt4Hov8qwIaVjefi5XkEYTxBVuN-EAUvW96S3g-ar6aKe-4l3E4)

------------



## Config.php 



```php
private $secret_id  = "xxxxxxxxxxxxxx";
private $secret_key = "xxxxxxxxxxxxxxxxxxxxxxx";
```

## CreateTransaction.php ( สร้างลิ้งชำระเงิน )

| พารามิเตอร์  | ความหมาย  | #  |
| ------------ | ------------ | ------------ |
| **secret_id**   |  Secret ID  | * จำเป็นต้องกรอก  |
| **secret_key**   | Secret Key   | * จำเป็นต้องกรอก |
| **firstname**   | ชื่อ   | ไม่จำเป็นต้องกรอก |
|  **lastname**  | นามสกุล   | ไม่จำเป็นต้องกรอก  |
|  **email** |  อีเมลล์ | ไม่จำเป็นต้องกรอก  |
|  **phone ** | เบอร์โทรศัพท์  | ไม่จำเป็นต้องกรอก |
| **amount **  | จำนวนเงิน  | * จำเป็นต้องกรอก  |
| **description **  | รายละเอียด  | ไม่จำเป็นต้องกรอก  |
| **address **  | ที่อยู่  | ไม่จำเป็นต้องกรอก  |
| **message **  | ข้อความ  | ไม่จำเป็นต้องกรอก  |
| **order_id **  | เลขที่ออเดอร์  | *  จำเป็นต้องกรอก และ ไม่สามารถกรอกซ้ำได้  |
| **success_Url **  | เมื่อชำระเงินสำเร็จจะ redirect มายัง url  | *  จำเป็นต้องกรอก |
| **fail_Url **  | เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url  | *  จำเป็นต้องกรอก|
| **cancel_Url **  | เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url  |  *  จำเป็นต้องกรอก |

#### Success Result :

```json
[
	{
		"status": "success",
		"transaction_ID": "xxxxxxxxxxxxxxxxxx",
		"link_payment": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	}
]
```

 - **transaction_ID : รหัสธุรกรรม**
 - **link_payment  : ลิ้งชำระเงิน**

#### Fail Result :

```json
[
	{
		"status": "error create"
	}
]
```


** # สร้างลิ้งชำระเงินไม่สำเร็จเพราะกรอกพารามิเตอร์ไม่ถูกต้อง**


------------

## Check_Transaction.php ( ตรวจสอบสถานะรหัสธุรกรรม )

| พารามิเตอร์  | ความหมาย  | #  |
| ------------ | ------------ | ------------ |
| **secret_id**   |  Secret ID  | จำเป็นต้องกรอก |
| **secret_key**   | Secret Key   | จำเป็นต้องกรอก |
|  **transaction_ID ** | รหัสธุรกรรม  | จำเป็นต้องกรอก  |


#### Success Result :

```json
[
	{
		"Amount ": "0.70"
		,"Transaction ID ": "MSTRF18000000012107",
		"Status Payment ": "pending",
		"Description ": ""
	}
]

```

 - **Amount  : จำนวนเงิน**
 - **Transaction ID   : รหัสธุรกรรม	 **
 - **Status Payment   : สถานะการชำระเงิน**
 - **Description   : รายละเอียด**

#### Fail Result :

```json
[
	{
		"status": "You cannot have permission to view this transaction."
	}
]

```

** # รหัสธุรกรรมของคุณไม่ถูกต้อง หรือ Secret ID , Secret Key ไม่ถูกต้อง**


------------

## Check_OrderID.php ( ตรวจสอบสถานะออเดอร์ )

| พารามิเตอร์  | ความหมาย  | #  |
| ------------ | ------------ | ------------ |
| **secret_id**   |  Secret ID  | จำเป็นต้องกรอก |
| **secret_key**   | Secret Key   | จำเป็นต้องกรอก |
|  **order_id  ** | เลขที่ออเดอร์  | จำเป็นต้องกรอก  |

#### Success Result :

```json
[
	{
		"Amount ": "0.70"
		,"Order ID ": "test_0001 ",
		"Status Payment ": "pending",
		"Description ": ""
	}
]

```

 - **Amount  : จำนวนเงิน**
 - **Order ID   : รหัสธุรกรรม	 **
 - **Status Payment   : สถานะการชำระเงิน**
 - **Description   : รายละเอียด**


#### Fail Result :

```json
[
	{
		"status": "You cannot have permission to view this order ."
	}
]

```

**# เลขที่ออเดอร์ของคุณไม่ถูกต้อง หรือ Secret ID , Secret Key ไม่ถูกต้อง**


------------



## Get_link_payment.php ( สร้างลิ้งชำระเงินจากรหัสธุรกรรม )

| พารามิเตอร์  | ความหมาย  | #  |
| ------------ | ------------ | ------------ |
| **secret_id**   |  Secret ID  | จำเป็นต้องกรอก  |
| **secret_key**   | Secret Key   | จำเป็นต้องกรอก |
|  **transaction_ID ** | รหัสธุรกรรม  | จำเป็นต้องกรอก  |


#### Success Result :
```json
[
	{
		"link_payment": "xxxxxxxxxxxxx"
	}
]

```
- **transaction_ID : รหัสธุรกรรม**



------------


## Webhook

```php
<?php
	$transectionID = $_POST["transectionID"];
	$amount = $_POST["amount"];
	$status = $_POST["status"];
	$orderid = $_POST["orderid"];
	$hash = $_POST["hash"];
?>
```
 - **Transaction ID   : รหัสธุรกรรม	 **
 - **Amount  : จำนวนเงิน**
 - **Status Payment   : สถานะการชำระเงิน**
 

------------

 ![enter image description here](https://lh3.googleusercontent.com/IZm13jwux7_HLTs9dUuAoE8fXlKEp4_KGsQ7OzkCEbf-yq4G9btKc0yG5IS2bv2MZsGV0gCx6rg)

