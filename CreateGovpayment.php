<?php 

require_once 'moneyspace/config/config.php';
require_once 'moneyspace/Api/Api.php';

use Moneyspace\Api\Api;
$config = new Config();
$SecretID = $config->getSecret_id();
$SecretKey = $config->getSecret_key();
$api = new Api();


$order_detail = array(
    ['email' => '', 'amount' => '0.50'],
    ['email' => '', 'amount' => '0.50']
);


$ms_data = array();
$ms_data["secret_id"] = $SecretID;
$ms_data["secret_key"] = $SecretKey;
$ms_data["firstname"] = ""; // ชื่อ
$ms_data["lastname"] = ""; // สกุล
$ms_data["email"] = ""; // อีเมลล์
$ms_data["phone"] = ""; // เบอร์โทรศัพท์
$ms_data["amount"] = "1.00"; // จำนวนเงิน
$ms_data["description"] = ""; // รายละเอียด
$ms_data["address"] = ""; // ที่อยู่
$ms_data["message"] = ""; // ข้อความ
$ms_data["order_id"] = ""; // เลขที่ออเดอร์
$ms_data["success_Url"] = "www.test.com"; // เมื่อชำระเงินสำเร็จจะ redirect มายัง url
$ms_data["fail_Url"] = "www.test.com"; // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
$ms_data["cancel_Url"] = "www.test.com"; // เมื่อชำระเงินไม่สำเร็จจะ redirect มายัง url
$ms_data["order_detail"] = json_encode($order_detail); // Email เจ้าหน้าที่


$Result = $api->CreateGovpayment($ms_data);
echo $Result;


?>


